package visitorSurp

import client.*

class VisitingClass: VisitorSurp {

    override fun visitBank(bank: Bank) {
        print("This is sending a message to Bank about theft insurance")
    }

    override fun visitResident(resident: Resident) {
        print("This is sending a message to resident")
    }

    override fun visitRestaurant(restaurant: Restaurant) {
        print("This is sending a message to restaurant")
    }

    override fun visitCompany(company: Company) {
        print("This is sending a message to company")
    }
}