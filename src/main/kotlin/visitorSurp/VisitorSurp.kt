package visitorSurp

import client.Bank
import client.Company
import client.Resident
import client.Restaurant



//Alternate solution without polymorphism. Also works in exactly the same way, but the appropriate
// method belonging to the particular class must be called in the double dispatch stage.
// It is better to implement the visitor in this way in some languages like C++ according to the
// pattern hatching book written by John Vlissides
interface VisitorSurp {

    fun visitBank(bank: Bank)

    fun visitResident(resident: Resident)

    fun visitRestaurant(restaurant: Restaurant)

    fun visitCompany(company: Company)
}