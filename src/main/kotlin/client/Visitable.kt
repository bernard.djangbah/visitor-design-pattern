package client

import visitor.Visiting

interface Visitable {
    fun accept(visiting: Visiting)
}