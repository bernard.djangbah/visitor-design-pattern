package client

import visitor.Visiting

class Resident(private val name: String,
               private val address: String,
               private val number: String):Client(name, address, number) {
//    override fun sendMail() {
//        println("I have sent an email as a resident")
//    }

    // Double dispatch in action
    override fun accept(visiting: Visiting) {
        visiting.visit(this)
    }
}