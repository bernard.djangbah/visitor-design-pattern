package client

import visitor.Visiting

class Company(private val name: String,
              private val address: String,
              private val number: String,
              private val numOfEmployees: Int): Client(name, address, number) {
//    override fun sendMail() {
//        println("Employees health insurance email sent to $name")
//    }

    // Double dispatch in action
    override fun accept(visiting: Visiting) {
        visiting.visit(this)
    }
}