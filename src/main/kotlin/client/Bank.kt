package client

import visitor.Visiting


class Bank(private val name: String,
           private val address: String,
           private val number: String):Client(name, address, number) {

    // Double dispatch in action
    override fun accept(visiting: Visiting) {
        visiting.visit(this)
    }
}