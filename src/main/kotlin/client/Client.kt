package client

import visitor.Visiting

abstract class Client(private val name: String,
                      private val address: String,
                      private val phoneNumber: String): Visitable {

    //    abstract fun sendMail();

// the open-close principle will be violated in this case since the sendMail method would be implemented
// in the individual subclasses. Each time the functions needs to be modified, we would need to open all
// subclasses and modify it in all of them. Instead, we want all modifications to be made outside the class.
// Same when we need to implement new features/functions too.
// We don't want to open all subclasses and then implement them. We want to do all in one place.
// Also, when you think about it, the insurance company's ability to send emails to clients isn't really a
// client behaviour, so it makes sense to not have the algorithm to send email in the individual classes.

    abstract override fun accept(visiting: Visiting)
}