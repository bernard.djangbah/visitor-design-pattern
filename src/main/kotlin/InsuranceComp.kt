import client.Client
import visitor.Visiting

class InsuranceComp(vararg clients: Client) {
    private var clientList = arrayListOf<Client>()

    init {
        for (client in clients)
            clientList.add(client)
    }

    public fun sendMailsToClients (visiting: Visiting) {
        for (client in clientList){
            client.accept(visiting)
        }
    }
}