package visitor

import client.*

interface Visitor {
    fun visit(bank: Bank)
    fun visit(company: Company)
    fun visit(resident: Resident)
    fun visit(restaurant: Restaurant)
}