package visitor

import client.*

class Visiting: Visitor {
// sendMessages function won't work because of how polymorphism works in Java/Kotlin, etc.
// Ahead of compile time, the jvm doesn't know the actual type of "client" so it can't call the right
// method for it.
// Some "functional" languages supports this though, like lisp.
// So here, we depend on "double dispatch" -> visitor passes itself to client and since the client class
// knows itself, it can pass itself to a visit method and the appropriate method could be called for it.

//    public fun sendMessages(clients: List<Client>){
//        for (client in clients)
//            visit(client)
//    }

    override fun visit(bank: Bank) {
        print("Sending a message to bank about theft insurance")
    }

    override fun visit(resident: Resident) {
        print("Sending a message to resident about house insurance")
    }

    override fun visit(restaurant: Restaurant) {
        print("Sending a message to restaurant about food insurance")
    }

    override fun visit(company: Company) {
        print("Sending a message to company about employee's health insurance")
    }
}