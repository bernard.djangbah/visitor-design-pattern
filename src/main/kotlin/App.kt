import client.Bank
import client.Company
import client.Resident
import client.Restaurant
import visitor.Visiting
import visitor.Visitor

fun main(args: Array<String>) {
    val bank = Bank("Stanbic Bank", "Ridge", "+2335577386")
    val turntabl = Company("turntabl", "Advantage Place", "+44455644361", 200)
    val restaurant = Restaurant("Ri's Food Place", "Banku Room", "+4427664914")
    val resident = Resident("Vincent's Villa", "Somewhere", "+4425843664296")
    val insuranceComp = InsuranceComp(bank, turntabl, restaurant, resident)
    val visitor = Visiting();

    insuranceComp.sendMailsToClients(visitor)
}